var blobs = [];
var randomBlobs = [];
var startRadius = 10;
var minSizeBlob = 4;
var idRandom = 0;

//Automatic Bubble Spawn Variables
var maxBubbles = 1000;
var maxXY = 1000;
var startSizeRandomBlobs = 4;

//Variables for Eat function
var eatTolerance = 0.1; //in Percent in Decimal

function Blob(id,name, x, y, radius, hue) {
  this.id = id;
  this.name = name;
  this.x = x;
  this.y = y;
  this.r = radius;
  this.hue = hue;
  this.saturation= 100;
  this.lightness = 50;
  this.velX = 0;
  this.velY = 0;
}

function ChangeRandomBlob(id, x, y) {
  this.id = id;
  this.x = x;
  this.y = y;
}

function distanceOfBlobs(blob1, blob2) {
  //x Linie
  if(blob1.x < blob2.x){
    x = blob2.x-blob1.x;
  }else {
    x = blob1.x-blob2.x;
  }

  //y Linie
  if(blob1.y < blob2.y){
    y = blob2.y-blob1.y;
  }else {
    y = blob1.y-blob2.y;
  }
  distance = Math.sqrt(Math.pow(x,2)+Math.pow(y,2));
  return distance;
}

function blobTouchesBlob (blob1,blob2){
  //if return < 0 then blobs touch each other
  return (distanceOfBlobs(blob1,blob2)-(blob1.r+blob2.r));
}

function radiusToVolume(radius){
  return Math.PI * Math.pow(radius,2);
}

function updateBlob(blob,data) {
  var velmouseX = data.x;
  var velmouseY = data.y;
  var newVelX = 0;
  var newVelY = 0;
  var distNewVelMouse = 0;
  var factorNewVel = 1;

  distNewVelMouse = vectorLength(velmouseX,velmouseY,data.w,data.h) * (2.5-((blob.r*2/maxXY)+0.1)*6);
  factorNewVel = Math.sqrt(Math.pow(distNewVelMouse,2)/(Math.pow(velmouseX,2)+Math.pow(velmouseY,2)));
  newVelX = velmouseX*factorNewVel;
  newVelY = velmouseY*factorNewVel;
  blob.velX=newVelX/4;
  blob.velY=newVelY/4;
}

function constrain(value,tolerance ,low,high){
  if(value - tolerance < low){
    return low + tolerance;
  }
  if(value + tolerance > high){
    return high - tolerance;
  }
  return value;
}

function vectorLength(x,y,width,height) {
  var posX = x/(width/2)*10;
  var posY = y/(height/2)*10;
  if (posX > 7){
    posX=7;
  }
  if(posY > 7){
    posY=7;
  }
  return Math.sqrt(Math.pow(posX,2)+Math.pow(posY,2));
}


function blobEatsBlob (bigBlob, smallBlob,checkPosition){

  // Wenn Blob1 größer ist als Blob2 und beide Blobs berühren sich
  if(bigBlob.r>smallBlob.r&&blobTouchesBlob(bigBlob,smallBlob)<0){

    // EatTolerance ist ein Prozentsatz.
    // Wenn Blob2 mehr als EatTolerance % kleiner als Blob1 ist
    if(1-(smallBlob.r/bigBlob.r)>eatTolerance){

      // Wenn die Distanz beider Blobs kleiner als der Radius des großen Blobs ist
      // (Also wenn der große Blob den kleinen Blob mehr als die Hälfte überdeckt)
      // oder der kleine Blob kleiner gleich die minimale Größe ist.
      if (distanceOfBlobs(bigBlob,smallBlob)<bigBlob.r || smallBlob.r<=minSizeBlob){
        // Neues Volumen des großen Blobs, um den neuen Radius zu berechnen
        // Neues Volumen = Volumen großer Blob + Volumen kleiner Blob
        if(checkPosition==false){
          var volumeBig = radiusToVolume(bigBlob.r) + radiusToVolume(smallBlob.r);
          bigBlob.r = Math.sqrt(volumeBig / Math.PI);
        }
        //srcSmallBlob.splice(srcSmallBlob.indexOf(smallBlob),1);
        // Da eine weitere Aktion erforderlich ist! (Löschen oder Ändern des kleinen Blobs)
        return true;
      } else {
        // Wenn der kleine Blob die Minimale Größe erreicht hat wird er ganz verschlungen
        if(smallBlob.r>minSizeBlob){
          if(checkPosition){
            return true;
          } else {
            var overlapSmallBlob = smallBlob.r-(distanceOfBlobs(bigBlob,smallBlob) - bigBlob.r);
            var volumeBig = radiusToVolume(bigBlob.r) + radiusToVolume(overlapSmallBlob);
            var volumeSmall = radiusToVolume(smallBlob.r) - radiusToVolume(overlapSmallBlob);
            bigBlob.r = Math.sqrt(volumeBig / Math.PI);
            smallBlob.r = Math.sqrt(volumeSmall / Math.PI);
          }
          // False da keine weitere Aktion erforderlich ist.
          return false;
        }
      }
    }
  }
}

// Using express: http://expressjs.com/
var express = require('express');
// Create the app
var app = express();

// Set up the server
// process.env.PORT is related to deploying on heroku
var server = app.listen(process.env.PORT || 3000, listen);

// This call back just tells us that the server has started
function listen() {
  var host = server.address().address;
  var port = server.address().port;
  console.log('BubbleBuffet listening at http://' + host + ':' + port);
}

app.use(express.static('public'));


// WebSocket Portion
// WebSockets work with the HTTP server
var io = require('socket.io')(server);

//initialize randomBlobs
while (maxBubbles>randomBlobs.length) {
  randomBlob = new Blob(idRandom,"",Math.round(Math.random()*(2*maxXY)-maxXY),Math.round(Math.random()*(2*maxXY)-maxXY),startSizeRandomBlobs,Math.round(Math.random()*359));
  randomBlobs.push(randomBlob);
  idRandom++;
}

setInterval(heartbeat, 16);

function heartbeat() {
  var changedRandomBlobs =[]
  //Player eats randomBlob
  for (bigBlob of blobs) {
    for (smallBlob of randomBlobs) {
      if (bigBlob!=smallBlob) {
        if(blobTouchesBlob(bigBlob,smallBlob)<eatTolerance){
          if(blobEatsBlob(bigBlob,smallBlob,false)){
            smallBlob.x = Math.round(Math.random()*(2*maxXY)-maxXY);
            smallBlob.y = Math.round(Math.random()*(2*maxXY)-maxXY);
            var changedSmallBlob = new ChangeRandomBlob(smallBlob.id, smallBlob.x, smallBlob.y);
            changedRandomBlobs.push(changedSmallBlob);
          }
        }
      }
    }
  }

  //Player eats another Player
  for (bigBlob of blobs) {
    for (smallBlob of blobs) {
      if (bigBlob!==smallBlob) {
        if(blobTouchesBlob(bigBlob,smallBlob)<eatTolerance){
          if(blobEatsBlob(bigBlob,smallBlob,false)){
            blobs.splice(blobs.indexOf(smallBlob),1);
          }
        }
      }
    }
  }

  //update position
  for (blob of blobs) {
    blob.x = Math.round(blob.x + blob.velX,-2);
    blob.x = constrain(blob.x,blob.r,-maxXY,maxXY);
    blob.y = Math.round(blob.y + blob.velY,-2);
    blob.y = constrain(blob.y,blob.r,-maxXY,maxXY);
  }


  io.sockets.emit('heartbeat_player', blobs);
  io.sockets.emit('heartbeat_random', changedRandomBlobs);
}



// Register a callback function to run when we have an individual connection
// This is run for each individual user that connects
io.sockets.on('connection',
  // We are given a websocket object in our function
  function(socket) {

    console.log("INFO: We have a new client:" + socket.id);
    socket.emit('init_random',randomBlobs);

    socket.on('start',
      function(data, callback) {
        var usernameUnique = true;
        for (user of blobs) {
          if(user.name == data.name){
            usernameUnique = false;
          }
        }
        if(usernameUnique){
          var getsEaten = true;
          var blob;
          while (getsEaten) {
            getsEaten=false;
            blob = new Blob(socket.id,data.name, Math.round((Math.random()*(2*maxXY))-maxXY), Math.round((Math.random()*(2*maxXY))-maxXY), startRadius, data.hue, 50, 100);
            for (bigBlob of blobs) {
              if(blobEatsBlob(bigBlob,blob,true)){
                getsEaten = true;
              }
            }
          }
          blobs.push(blob);
          callback(true);
          console.log("INFO: User "+data.name+" logged in");
        } else {
          callback(false);
        }

      }
    );

    socket.on('update',
      function(data) {
        var blob;
        for (var i = 0; i < blobs.length; i++) {
          if (socket.id == blobs[i].id) {
            blob = blobs[i];
            updateBlob(blob,data);
          }
        }
      }
    );



    socket.on('disconnect', function() {
      console.log("INFO: Client has disconnected: " + socket.id);
      for (var i = 0; i < blobs.length; i++) {
        if (socket.id == blobs[i].id) {
          console.log("INFO: User "+blobs[i].name+" logged out");
      blobs.splice(i,1);
        }
      }
    });
  }
);

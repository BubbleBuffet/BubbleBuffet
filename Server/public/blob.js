function Blob(x, y, r, id) {
  this.pos = createVector(x, y);
  this.r = r;
  this.vel = createVector(0, 0);
  this.id = id;
  this.hue = round(random(0,359));


  this.update = function() {
    var velmouse = createVector(mouseX - width / 2, mouseY - height / 2);
    var newVel = createVector(0,0);
    var distNewVelMouse = 0;
    var factorNewVel = 1

    distNewVelMouse = vectorLength(velmouse) * (2.5-((this.r*2/1000)+0.1)*6);
    factorNewVel = Math.sqrt(Math.pow(distNewVelMouse,2)/(Math.pow(velmouse.x,2)+Math.pow(velmouse.y,2)));
    newVel = velmouse.mult(factorNewVel);
    console.log(newVel);
    this.pos.add(newVel);
  }

  this.eats = function(other) {
    var d = p5.Vector.dist(this.pos, other.pos);
    if (d < this.r + other.r && this.r-other.r>5) {
      var sum = PI * this.r * this.r + PI * other.r * other.r;
      this.r = sqrt(sum / PI);
      //this.r += other.r;
      return true;
    } else {
      return false;
    }
  }

  this.constrain = function(borderWidth,borderHeight) {
    blob.pos.x = constrain(blob.pos.x, -borderWidth+this.r, borderWidth-this.r);
    blob.pos.y = constrain(blob.pos.y, -borderHeight+this.r, borderHeight-this.r);
  }

  this.show = function() {
    c = color(String("hsl("+this.hue+", 100% , 50%)"));
    fill(c);
    //fill(0,255,0);
    ellipse(this.pos.x, this.pos.y, this.r * 2, this.r * 2);

  }

}

function vectorLength(vector) {
  var pos = createVector(vector.x/(width/2)*10,vector.y/(height/2)*10)
  pos.limit(5);
  return Math.sqrt(Math.pow(pos.x,2)+Math.pow(pos.y,2));
}

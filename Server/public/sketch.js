// Keep track of our socket connection
var socket;
var framerate = 35;
var canvas;
var indexOfBlob;
var blob;
var blobs = [];
var randomBlobs = [];
var zoom = 1;
var nickname="";
var login= false;
var canvasWidth;
var canvasHeight;
var stillAlive = false;
var firstTimeEmitted = false;
var maxPoints=0;
var playerInRanking=false;
var fullscreenToggle=false;
var firstDraw=false;

var maxRadius = 1000; //getMaxRadius()=math.min({(math.abs(MaxPosX)+math.abs(MinPosX))/2, (math.abs(MaxPosY)+math.abs(MinPosY))/2 })

function setup() {
  frameRate(framerate);
  nickname = "";
  canvasWidth = windowWidth;
  canvasHeight = windowHeight;

  // Start a socket connection to the server
  // Some day we would run this server somewhere else
  socket = io.connect('localhost:3000');

  //blob = new Blob(random(-canvasWidth, canvasWidth), random(-canvasHeight, canvasHeight), random(8, 24));
  socket.on('init_random',
    function(data) {
      randomBlobs = data;
    }
  );

  socket.on('heartbeat_player',
    function(data) {
      blobs = data;
      blobs.sort(function compareBlobs(a,b){
        var volA = radiusToVolume(a.r);
        var volB = radiusToVolume(b.r);

        if(volA < volB){
          return 1;
        }
        if(volA > volB){
          return -1;
        }
        return 0;
      });
      var tmpLiving = false;
      for (bubble of data) {
        if(socket.id == bubble.id){
          tmpLiving = true;
          if(firstTimeEmitted==false){
            firstTimeEmitted=true;
          }
        }
      }
      stillAlive=tmpLiving;
    }

  );

  socket.on('heartbeat_random',
    function(data) {
      var tmpBlobs = data;
      for (changedRandomBlob of tmpBlobs) {
        for (randomBlob of randomBlobs) {
          if(randomBlob.id == changedRandomBlob.id){
            randomBlob.x = changedRandomBlob.x;
            randomBlob.y = changedRandomBlob.y;
          }
        }
      }
    }
  );

}

function coordLeaderboard(position){
  var coord = createVector(blob.x,blob.y);
  coord.x = coord.x + ((canvasWidth/2-15)/zoom);
  coord.y = coord.y - (((canvasHeight/2)-(25*position))/zoom);
  return coord;
}

function radiusToVolume(radius){
  if(radius<0){
    return 0;
  }
  return Math.PI * Math.pow(radius,2);
}

function playAgain(){
  maxPoints=0;
  login=true;
  stillAlive=true;
  firstTimeEmitted=false;
  createCanvas(canvasWidth, canvasHeight);
  //blob = new Blob(random(-1000,1000), random(-1000,1000), 16, socket.id);
  // Make a little object with  and y
  var data = {
    x: Math.round(random(-1000,1000)),
    y: Math.round(random(-1000,1000)),
    r: 16,
    hue: Math.round(random(0,359)),
    name: nickname
  };

  socket.emit('start', data, function(usernameUnique){
    if(usernameUnique){
      var main = document.getElementById("main");
      main.style.display = 'none';
      login=true;
      stillAlive = true;
    }else {
      var nameAllreadyUsed = document.getElementById("nameAllreadyUsed");
      nameAllreadyUsed.style.display = 'inline';
    }
  });
}

  function user(){
    var noNickname = document.getElementById("noNickname");
    noNickname.style.display = 'none';
    var nameAllreadyUsed = document.getElementById("nameAllreadyUsed");
    nameAllreadyUsed.style.display = 'none';
    nickname = document.getElementById('username').value;
    if(nickname!=""&&nickname.length <= 12){
      login=true;
      canvas = createCanvas(canvasWidth, canvasHeight);
      //blob = new Blob(random(-1000,1000), random(-1000,1000), 16, socket.id);
      // Make a little object with  and y
      var data = {
        x: Math.round(random(-1000,1000)),
        y: Math.round(random(-1000,1000)),
        r: 16,
        hue: Math.round(random(0,359)),
        name: nickname
      };

    socket.emit('start', data, function(usernameUnique){
      if(usernameUnique){
        var main = document.getElementById("main");
        main.style.display = 'none';
        login=true;
        stillAlive = true;
      }else {
        var nameAllreadyUsed = document.getElementById("nameAllreadyUsed");
        nameAllreadyUsed.style.display = 'inline';
      }
    });
  }else {
    var noNickname = document.getElementById("noNickname");
    noNickname.style.display = 'inline';
  }
}

function keyPressed() {
  if (keyCode === 32) {
    if(stillAlive==false && login==true && firstTimeEmitted==true){
      playAgain();
    }
  }
  if (keyCode === 70) {
    if(login==true) {
      fullscreenToggle = fullscreen();
      fullscreen(!fullscreenToggle);
    }

  }
}

function windowResized() {
  canvasWidth = windowWidth;
  canvasHeight = windowHeight;
  resizeCanvas(canvasWidth, canvasHeight);
}

function fillHsl(hue, saturation, lightness){
  c = color(String("hsl("+hue+", "+saturation+"% ,"+lightness+"%)"));
  fill(c);

}

function writeText(hue, saturation, lightness, size, align,style,yourtext,x,y){
  fillHsl(hue, saturation, lightness);
  textSize(size);
  textAlign(align);
  textStyle(style);
  text(yourtext,x,y);
}



function draw() {
  if(login){
    if(stillAlive){
      indexOfBlob = blobs.map(function(e) { return e.id; }).indexOf(socket.id);
      if(indexOfBlob>=0){
        if (firstDraw==false){
          blob = blobs[indexOfBlob];
          newblob = blobs[indexOfBlob];
        } else {
          if(oldblob != blob){
            blob.x=lerp(blob.x,newblob.x,0.2);
            blob.y=lerp(blob.y,newblob.y,0.2);
            blob.r=lerp(blob.r,newblob.r,0.2);
          }
        }
        background(255);
        stroke(255);
        translate(width / 2, height / 2);
        var newzoom = 16 / (Math.sqrt(blob.r));
        zoom = lerp(zoom, newzoom, 0.1);
        scale(zoom);
        translate(-blob.x, -blob.y);
        for (var i = randomBlobs.length - 1; i >= 0; i--) {
            fillHsl(randomBlobs[i].hue , 100, 50);
            //fill(0, 0, 255);
            ellipse(randomBlobs[i].x, randomBlobs[i].y, randomBlobs[i].r * 2, randomBlobs[i].r * 2);
        }

        for (var i = blobs.length - 1; i >= 0; i--) {
          var id = blobs[i].id;
          if (id != socket.id) {
            fill(blobs[i].hue , 100, 50);
            //fill(0, 0, 255);

            ellipse(blobs[i].x, blobs[i].y, blobs[i].r * 2, blobs[i].r * 2);

            writeText(0,0,0,30/zoom,CENTER,NORMAL,blobs[i].name, blobs[i].x, blobs[i].y+2);
          }else {
            fill(blobs[i].hue , 100, 50);
            //fill(0, 0, 255);

            ellipse(blobs[i].x, blobs[i].y, blobs[i].r * 2, blobs[i].r * 2);

            writeText(0,0,0,30/zoom,CENTER,NORMAL,blobs[i].name, blobs[i].x, blobs[i].y+2);
            blob.r = lerp(blob.r,blobs[i].r,0.1);
          }
        }


        // Bisher noch fehlerhaft! Siehe Issue!
        playerInRanking=false;
        for (var i = 0; i < Math.min(blobs.length,10); i++) {
          var coord;
          if(i==0){
            coord = coordLeaderboard(i+1);
            writeText(0,0,0,25/zoom,RIGHT,BOLD,"Leaderboard",coord.x,coord.y);
          }
          coord = coordLeaderboard(i+3);
          var position = i+1;
          var points = Math.round(radiusToVolume(blobs[i].r-10));

          if(socket.id==blobs[i].id){
            writeText(0,0,0,25/zoom,RIGHT,BOLD,String(position+". "+blobs[i].name+" - "+points),coord.x,coord.y);
            playerInRanking=true;
            maxPoints=Math.max(maxPoints,points);
          } else {
            writeText(0,0,0,25/zoom,RIGHT,NORMAL,String(position+". "+blobs[i].name+" - "+points),coord.x,coord.y);
          }
        }

        if(playerInRanking==false&firstTimeEmitted){
          var coord = coordLeaderboard(13);
          var position = blobs.map(function(e) { return e.id; }).indexOf(socket.id)+1;
          var points = Math.round(radiusToVolume(blobs[position-1].r-10));
          maxPoints=Math.max(maxPoints,points);
          writeText(0,0,0,25/zoom,RIGHT,BOLD,String(position+". "+blobs[position-1].name+" - "+points),coord.x,coord.y);
        }

        //blob.show();
        //if (mouseIsPressed) {
          //blob.update();
        //}
        //blob.constrain(1000, 1000);

        var data = {
          x: Math.round(mouseX - width / 2,-2),
          y: Math.round(mouseY - height / 2,-2),
          w: width,
          h: height
        };
        socket.emit('update', data);

      }

    } else {
      if(firstTimeEmitted){
        background(255);
        stroke(255);
        scale(1);

        writeText(0,100,50,canvasWidth/10,CENTER,BOLD,"Game Over", canvasWidth/2, canvasHeight*(1/3));
        writeText(0,0,0,canvasWidth/50,CENTER,NORMAL,"Press Space for joining the Game again!",canvasWidth/2, canvasHeight*(1/3)+2*canvasWidth/50);
        writeText(0,0,0,canvasWidth/40,CENTER,BOLD,nickname,canvasWidth/2,canvasHeight*(1/3)+2*canvasWidth/50+2*canvasWidth/40);
        writeText(0,0,0,canvasWidth/40,CENTER,BOLD,maxPoints,canvasWidth/2,canvasHeight*(1/3)+2*canvasWidth/50+4*canvasWidth/40);

      }
    }
  }
}

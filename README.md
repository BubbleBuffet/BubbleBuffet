# BubbleBuffet

BubbleBuffet ist ein Agar.io Clone, der als Projekt für die Vorlesung Webengineering II an der DHBW Mannheim von Joshua Töpfer und Florian Proske entwickelt wurde. Eine lauffähige Version von BubbleBuffet ist unter [bubblebuffet.de](http://bubblebuffet.de) erreichbar.

## Installation
1. Node.js installieren [hier herunterladen](https://nodejs.org/en/download/)
2. Git installieren [hier herunterladen](https://git-scm.com/downloads)
3. Eine git-fähige Sonsole öffnen und das Bubblebuffet Repository clonen. (mit dem Commando `git clone https://gitlab.com/BubbleBuffet/BubbleBuffet`)
4. Öffne eine Node.js-fähige Console und gib folgendes Commando ein: `npm install -g socket.io express`
5. Navigiere innerhalb der Console in den Ordner `/BubbleBuffet/Server`
6. Gebe in der Console `npm install` ein.

Die Anwendung ist nun einsatzbereit. Mit dem Commande `node server` kann der Server gestartet werden. Die Anwendung ist nun unter `localhost:3000` erreichbar.
